browsers_to_ignore = [
        "opera",
        "firefox",
        "yandex-browser",
        "android",
        "safari",
        "edge",
        "chrome",
        "internet-explorer",
    ]
def ignore_browsers(page):
    if page in browsers_to_ignore:
        return "index"
    else:
        return page
